import { gql } from "@apollo/client";

export default gql`
  query items {
    items {
      id
      image {
        url
        previewUrl
      }
      title
      description
      price
    }
  }
`;
