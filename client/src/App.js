import { useQuery } from "@apollo/client";
import GET_ITEMS from "./gql/query/items";

const App = () => {
  const { loading, data } = useQuery(GET_ITEMS);
  console.log(data);
  if (loading) return <div>loading...</div>;
  return (
    <>
      {data.items.map((item) => {
        return <div key={item.id}>{item.title}</div>;
      })}
    </>
  );
};

export default App;
